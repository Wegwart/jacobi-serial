# Jacobi Serial

# Create Solution
(.venv) λ md build && cd build && cmake -G "Visual Studio 16 2019" ..
or
$ mkdir build && cd build && cmake ..
$ cmake --build .


Test Jacobi Method (i5-6200U CPU @ 2.30GHz)

HelperTest.Run...
Passed
JacobiTest.Run...
Let Ax = b
A: [[5 2 1 1]
[2 6 2 1]
[1 2 7 1]
[1 1 2 8]]
b: [29 31 26 19]
Run jacobi...
epsilon: 1e-10
maxIterations: 500
xInit: [0 0 0 0]
--- 0 seconds ---
xResult: [3.99275 2.95411 2.16184 0.966184]
bComputed: [29 31 26 19]
assert: bError 2.93812e-10 < MaxError 1e-08
Passed

D:\work\jacobi-serial\Cpp\build\Debug\JacobiSerial.exe (process 7744) exited with code 0.
Press any key to close this window . . .
