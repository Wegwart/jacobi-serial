﻿#pragma once

#include <iostream>
#include <sstream>
#include <cmath> 
#include <stdexcept>

using namespace std;

namespace JacobiSerial
{
    namespace Helper
    {
        template <size_t rows>
        inline void Copy(const double(&a)[rows], double(&res)[rows])
        {
            for (int i = 0; i < rows; i++)
            {
                res[i] = a[i];
            }
        }

        template <size_t rows, size_t cols>
        inline void Dot(const double(&a)[rows][cols], const double(&b)[rows], double(&res)[rows])
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                    res[i] += a[i][j] * b[j];
            }
        }

        template <size_t rows>
        inline double Norm(const double(&a)[rows])
        {
            double res = 0;
            for (int i = 0; i < rows; i++)
            {
                res += a[i] * a[i];
            }
            return sqrt(res);
        }

        template <size_t rows, size_t cols>
        inline void Diag(const double(&a)[rows][cols], double(&res)[rows])
        {
            for (int i = 0; i < rows; i++)
            {
                res[i] = a[i][i];
            }
        }

        template <size_t rows, size_t cols>
        inline void Diag(const double(&a)[rows], double(&res)[rows][cols])
        {
            for (int i = 0; i < rows; i++)
            {
                res[i][i] = a[i];
            }
        }

        template <size_t rows, size_t cols>
        inline void Sub(const double(&a)[rows][cols], const double(&b)[rows][cols], double(&res)[rows][cols])
        {
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    res[i][j] = a[i][j] - b[i][j];
                }
            }
        }

        template <size_t rows>
        inline void Sub(const double(&a)[rows], const double(&b)[rows], double(&res)[rows])
        {
            for (int i = 0; i < rows; i++)
            {
                res[i] = a[i] - b[i];
            }
        }

        template <size_t rows>
        inline void Inv(const double(&a)[rows], double(&res)[rows])
        {
            for (int i = 0; i < rows; i++)
            {
                res[i] = 1 / a[i];
            }
        }

        template <size_t rows>
        inline string ToString(const double(&a)[rows])
        {
            stringstream ss;
            ss << "[";
            for (int i = 0; i < rows; i++)
            {
                ss << a[i];
                if (i + 1 < rows)
                    ss << " ";
            }
            ss << "]";
            return ss.str();
        }

        template <size_t rows, size_t cols>
        inline string ToString(const double(&a)[rows][cols])
        {
            stringstream ss;
            ss << "[";
            for (int i = 0; i < rows; i++)
            {
                ss << ToString(a[i]);
                if (i + 1 < rows)
                    ss << endl;
            }
            ss << "]";
            return ss.str();
        }

        inline bool Equal(const double a, const double b, const double epsilon)
        {
            if (abs(b - a) > epsilon)
                return false;
            return true;
        }

        template <size_t rows>
        inline bool Equal1D(const double(&a)[rows], const double(&b)[rows], const double epsilon)
        {
            for (int i = 0; i < rows; i++)
            {
                if (abs(b[i] - a[i]) > epsilon)
                    return false;
            }
            return true;
        }

        template <size_t rows, size_t cols>
        inline bool Equal2D(const double(&a)[rows][cols], const double(&b)[rows][cols], const double epsilon)
        {
            for (int i = 0; i < rows; i++)
            {
                if (!Equal1D(a[i], b[i], epsilon))
                    return false;
            }
            return true;
        }

        inline void Assert(bool condition)
        {
            if (!condition)
                throw new runtime_error("Failed");
        }
    }
}
