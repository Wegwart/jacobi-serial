﻿#pragma once

#include <iostream>
#include <memory>

using namespace std;

namespace JacobiSerial
{
    class HelperTest
    {
        public:

        static void Run();
        static void Dot_Sample_AreEqual();
        static void Norm_Sample1D_AreEqual();
        static void Diag_Sample2D_AreEqual();
        static void Diag_Sample1D_AreEqual();
        static void Sub_Sample1D_AreEqual();
        static void Sub_Sample2D_AreEqual();
        static void Inv_Sample1D_AreEqual();
    };
}
