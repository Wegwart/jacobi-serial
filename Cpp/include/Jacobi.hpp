﻿#pragma once

#include "Helper.hpp"

#include <iostream>
#include <memory>

using namespace std;

namespace JacobiSerial
{
    namespace Jacobi
    {
        const double Epsilon = 1e-10;
        const int MaxIterations = 500;

        template <size_t rows, size_t cols>
        void Jacobi(const double(&A)[rows][cols], const double(&b)[rows], const double(&xInit)[rows], const double epsilon, const int maxIterations, double(&res)[rows])
        {
            double D1D[rows]{};
            Helper::Diag(A, D1D);
            double D[rows][cols]{};
            Helper::Diag(D1D, D);
            double LU[rows][cols]{};
            Helper::Sub(A, D, LU);
            double x[rows]{};
            Helper::Copy(xInit, x);
            double D1DInv[rows]{};
            double DInv[rows][cols]{};
            Helper::Inv(D1D, D1DInv);
            Helper::Diag(D1DInv, DInv);
            double xNew[rows]{};
            for (int i = 0; i < maxIterations; i++)
            {
                double LUx[rows]{};
                Helper::Dot(LU, x, LUx);
                double bSubLUx[rows]{};
                Helper::Sub(b, LUx, bSubLUx);
                double xNew[rows]{};
                Helper::Dot(DInv, bSubLUx, xNew);
                double xNewSubx[rows]{};
                Helper::Sub(xNew, x, xNewSubx);
                if (Helper::Norm(xNewSubx) < epsilon)
                {
                    Helper::Copy(xNew, res);
                    return;
                }
                Helper::Copy(xNew, x);
            }
        }
    }
}
