﻿#pragma once

#include<iostream>

#include  <memory>

using namespace std;

namespace JacobiSerial
{
    class JacobiTest
    { 
        public:

        static void Run();
        static void Jacobi_Sample_ErrorCheckOk();
    };
}
