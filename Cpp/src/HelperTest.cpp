﻿#include "Helper.hpp"
#include "HelperTest.hpp"

#include <iostream>
#include <memory>
#include <cmath>

using namespace std;

namespace JacobiSerial
{
    void HelperTest::Run()
    {
        cout << "HelperTest.Run..." << endl;
        Dot_Sample_AreEqual();
        Norm_Sample1D_AreEqual();
        Diag_Sample2D_AreEqual();
        Diag_Sample1D_AreEqual();
        Sub_Sample1D_AreEqual();
        Sub_Sample2D_AreEqual();
        Inv_Sample1D_AreEqual();
        cout << "Passed" << endl;
    }

    void HelperTest::Dot_Sample_AreEqual()
    {
        const double A[][3] {
            { 1, 2, 3},
            { 4, 5, 6},
            { 7, 8, 9},
        };
        const double B[] { 1, 1, 1 };
        const double expectedC[] { 1 + 2 + 3, 4 + 5 + 6, 7 + 8 + 9 };
        double actualC[3] {};
        Helper::Dot(A, B, actualC);

        Helper::Assert(Helper::Equal1D(expectedC, actualC, 1e-6));
    }

    void HelperTest::Norm_Sample1D_AreEqual()
    {
        const double A[] { 1 / 1, 1 / 2, 1 / 3 };
        const double expectedB = sqrt(1 / 1 + 1 / 4 + 1 / 9);

        auto actualB = Helper::Norm(A);

        Helper::Assert(Helper::Equal(expectedB, actualB, 1e-6));
    }

    void HelperTest::Diag_Sample2D_AreEqual()
    {
        const double A[][3] {
            { 1, 2, 3},
            { 4, 5, 6},
            { 7, 8, 9},
        };
        const double expectedB[] { 1, 5, 9 };
        double actualB[3] {};
        Helper::Diag(A, actualB);

        Helper::Assert(Helper::Equal1D(expectedB, actualB, 1e-6));
    }

    void HelperTest::Diag_Sample1D_AreEqual()
    {
        const double A[] { 1, 5, 9 };
        const double expectedB[][3] {
            { 1, 0, 0},
            { 0, 5, 0},
            { 0, 0, 9},
        };
        double actualB[3][3]{};
        Helper::Diag(A, actualB);

        Helper::Assert(Helper::Equal2D(expectedB, actualB, 1e-6));
    }

    void HelperTest::Sub_Sample1D_AreEqual()
    {
        const double A[] { 1, 2 };
        const double B[] { 3, 4 };
        const double expectedC[] { 1 - 3, 2 - 4 };
        double actualC[2]{};
        Helper::Sub(A, B, actualC);

        Helper::Assert(Helper::Equal1D(expectedC, actualC, 1e-6));
    }

    void HelperTest::Sub_Sample2D_AreEqual()
    {
        const double A[][2] {
            { 1, 2},
            { 3, 4},
        };
        const double B[][2] {
            { 5, 6},
            { 7, 8},
        };
        double expectedC[][2] {
            { 1-5, 2-6},
            { 3-7, 4-8},
        };
        double actualC[2][2]{};
        Helper::Sub(A, B, actualC);

        Helper::Assert(Helper::Equal2D(expectedC, actualC, 1e-6));
    }

    void HelperTest::Inv_Sample1D_AreEqual()
    {
        const double A[] { 1, 5, 9 };
        const double expectedB[] { 1 / 1.0, 1 / 5.0, 1 / 9.0 };
        double actualB[3]{};
        Helper::Inv(A, actualB);

        Helper::Assert(Helper::Equal1D(expectedB, actualB, 1e-6));
    }
}
