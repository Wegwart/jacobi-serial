﻿#include "JacobiTest.hpp"
#include "Helper.hpp"
#include "Jacobi.hpp"

#include <iostream>
#include <iomanip>
#include <memory>
#include <chrono>

using namespace std;

namespace JacobiSerial
{ 
    void JacobiTest::Run()
    {
        cout << "JacobiTest.Run..." << endl;
        Jacobi_Sample_ErrorCheckOk();
        cout << "Passed" << endl;
    }

    void JacobiTest::Jacobi_Sample_ErrorCheckOk()
    {
        // problem data
        const double A[][4] {
            { 5, 2, 1, 1},
            { 2, 6, 2, 1},
            { 1, 2, 7, 1},
            { 1, 1, 2, 8},
        };
        const double b[] { 29, 31, 26, 19 };
        cout << "Let Ax = b" << endl;
        cout << "A: " << Helper::ToString(A) << endl;
        cout << "b: " << Helper::ToString(b) << endl;

        cout << "Run jacobi..." << endl;
        const double epsilon = 1e-10;
        const int maxIterations = 500;

        // you can choose any starting vector
        const double xInit[] { 0, 0, 0, 0 };
        cout << "epsilon: " << epsilon << endl;
        cout << "maxIterations: " << maxIterations << endl;
        cout << "xInit: " << Helper::ToString(xInit) << endl;
        auto startTime = chrono::steady_clock::now();
        double xResult[4] {};
        Jacobi::Jacobi(A, b, xInit, epsilon, maxIterations, xResult);
        cout << "--- " << chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - startTime).count()/1000.0 << " seconds ---" << endl;

        double bComputed[4]{};
        Helper::Dot(A, xResult, bComputed);
        cout << "xResult: " << Helper::ToString(xResult) << endl;
        cout << "bComputed: " << Helper::ToString(bComputed) << endl;
        double bDiff[4]{};
        Helper::Sub(bComputed, b, bDiff);
        auto bError = Helper::Norm(bDiff);
        const double maxError = 1e-8;
        cout << "assert: bError " << bError << " < MaxError " << maxError << endl;

        Helper::Assert(bError < maxError);
    }
}
