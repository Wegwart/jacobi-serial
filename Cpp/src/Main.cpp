﻿#include "HelperTest.hpp"
#include "JacobiTest.hpp"

using namespace std;
using namespace JacobiSerial;

int main() 
{ 
    HelperTest::Run();
    JacobiTest::Run();
    return 0;
} 