# Jacobi Serial

Test Jacobi Method 


(.venv) λ python main.py
Let Ax = b
A: [[5 2 1 1]
 [2 6 2 1]
 [1 2 7 1]
 [1 1 2 8]]
b: [29 31 26 19]
Run jacobi...
epsilon: 1e-10
max_iterations: 500
x_init: [0. 0. 0. 0.]
--- 0.0 seconds ---
x_result: [3.99275362 2.95410628 2.16183575 0.96618357]
b_computed: [29. 31. 26. 19.]
assert: b_error 2.938101548046738e-10 < max_error 1e-08
Passed