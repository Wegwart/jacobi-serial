﻿using System;

namespace JacobiSerial
{
    public class Helper
    {
        public static double[] Dot(double[][] a, double[] b)
        {
            var dot = new double[a.Length];

            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < b.Length; j++)
                    dot[i] += a[i][j] * b[j];
            }
            return dot;
        }

        public static double Norm(double[] a)
        {
            double res = 0;
            for (int i = 0; i < a.Length; i++)
            {
                res += a[i] * a[i];
            }
            return Math.Sqrt(res);
        }

        public static double[] Diag(double[][] a)
        {
            var res = new double[a.Length];
            for (int i = 0; i < a.Length; i++)
            {
                res[i] = a[i][i];
            }
            return res;
        }

        public static double[][] Diag(double[] a)
        {
            var res = new double[a.Length][];
            for (int i = 0; i < a.Length; i++)
            {
                res[i] = new double[a.Length];
                res[i][i] = a[i];
            }
            return res;
        }

        public static double[][] Sub(double[][] a, double[][] b)
        {
            var res = new double[a.Length][];
            for (int i = 0; i < a.Length; i++)
            {
                res[i] = new double[a[0].Length];
                for (int j = 0; j < a[0].Length; j++)
                {
                    res[i][j] = a[i][j] - b[i][j];
                }
            }
            return res;
        }

        public static double[] Sub(double[] a, double[] b)
        {
            var res = new double[a.Length];
            for (int i = 0; i < a.Length; i++)
            {
                res[i] = a[i] - b[i];
            }
            return res;
        }

        public static double[] Inv(double[] a)
        {
            var res = new double[a.Length];
            for (int i = 0; i < a.Length; i++)
            {
                res[i] = 1 / a[i];
            }
            return res;
        }

        public static string ToString(double[] a)
        {
            return string.Format("[{0}]", string.Join(" ", a));
        }

        public static string ToString(double[][] a)
        {
            var result = new String[a.Length];
            for (int i = 0; i < a.Length; i++)
            {
                result[i] = ToString(a[i]);
            }
            return string.Format("[{0}]", string.Join("\n", result));
        }

        public static bool Equal(double a, double b, double epsilon)
        {
            if (Math.Abs(b - a) > epsilon)
                    return false;
            return true;
        }

        public static bool Equal(double[] a, double[] b, double epsilon)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (Math.Abs(b[i] - a[i]) > epsilon)
                    return false;
            }
            return true;
        }

        public static bool Equal(double[][] a, double[][] b, double epsilon)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (!Equal(a[i], b[i], epsilon))
                    return false;
            }
            return true;
        }

        public static void Assert(bool condition)
        {
            if (!condition)
                throw new Exception("Failed");
        }
    }

}
