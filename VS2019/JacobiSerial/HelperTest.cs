﻿using System;

namespace JacobiSerial
{
    public class HelperTest
    {
        public static void Run()
        {
            Console.WriteLine("HelperTest.Run...");
            Dot_Sample_AreEqual();
            Norm_Sample1D_AreEqual();
            Diag_Sample2D_AreEqual();
            Diag_Sample1D_AreEqual();
            Sub_Sample1D_AreEqual();
            Sub_Sample2D_AreEqual();
            Inv_Sample1D_AreEqual();
            Console.WriteLine("Passed");
        }

        public static void Dot_Sample_AreEqual()
        {
            var A = new double[][] {
                new double[] { 1, 2, 3},
                new double[] { 4, 5, 6},
                new double[] { 7, 8, 9},
            };
            var B = new double[] { 1, 1, 1 };
            var expectedC = new double[] { 1 + 2 + 3, 4 + 5 + 6, 7 + 8 + 9 };

            var actualC = Helper.Dot(A, B);

            Helper.Assert(Helper.Equal(expectedC, actualC, 1e-6));
        }

        public static void Norm_Sample1D_AreEqual()
        {
            var A = new double[] { 1 / 1, 1 / 2, 1 / 3 };
            var expectedB = Math.Sqrt(1 / 1 + 1 / 4 + 1 / 9);

            var actualB = Helper.Norm(A);

            Helper.Assert(Helper.Equal(expectedB, actualB, 1e-6));
        }

        public static void Diag_Sample2D_AreEqual()
        {
            var A = new double[][] {
                new double[] { 1, 2, 3},
                new double[] { 4, 5, 6},
                new double[] { 7, 8, 9},
            };
            var expectedB = new double[] { 1, 5, 9 };

            var actualB = Helper.Diag(A);

            Helper.Assert(Helper.Equal(expectedB, actualB, 1e-6));
        }

        public static void Diag_Sample1D_AreEqual()
        {
            var A = new double[] { 1, 5, 9 };
            var expectedB = new double[][] {
                new double[] { 1, 0, 0},
                new double[] { 0, 5, 0},
                new double[] { 0, 0, 9},
            };

            var actualB = Helper.Diag(A);

            Helper.Assert(Helper.Equal(expectedB, actualB, 1e-6));
        }

        public static void Sub_Sample1D_AreEqual()
        {
            var A = new double[] { 1, 2 };
            var B = new double[] { 3, 4 };
            var expectedC = new double[] { 1 - 3, 2 - 4 };

            var actualC = Helper.Sub(A, B);

            Helper.Assert(Helper.Equal(expectedC, actualC, 1e-6));
        }

        public static void Sub_Sample2D_AreEqual()
        {
            var A = new double[][] {
                new double[] { 1, 2},
                new double[] { 3, 4},
            };
            var B = new double[][] {
                new double[] { 5, 6},
                new double[] { 7, 8},
            };
            var expectedC = new double[][] {
                new double[] { 1-5, 2-6},
                new double[] { 3-7, 4-8},
            };

            var actualC = Helper.Sub(A, B);

            Helper.Assert(Helper.Equal(expectedC, actualC, 1e-6));
        }

        public static void Inv_Sample1D_AreEqual()
        {
            var A = new double[] { 1, 5, 9 };
            var expectedB = new double[] { 1f / 1, 1f / 5, 1f / 9 };

            var actualB = Helper.Inv(A);

            Helper.Assert(Helper.Equal(expectedB, actualB, 1e-6));
        }
    }
}
