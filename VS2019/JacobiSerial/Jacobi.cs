﻿namespace JacobiSerial
{
    //#
    //# https://en.wikipedia.org/wiki/Jacobi_method
    //#
    //def jacobi(A, b, x_init, epsilon= 1e-10, max_iterations= 500) :
    //    D = np.diag(np.diag(A))
    //    LU = A - D
    //    x = x_init
    //    D_inv = np.diag(1 / np.diag(D))
    //    for i in range(max_iterations) :
    //        x_new = np.dot(D_inv, b - np.dot(LU, x))
    //        if np.linalg.norm(x_new - x) < epsilon:
    //            return x_new
    //        x = x_new
    //    return x
    //
    public static class JacobiSerial
    {
        public static double[] Jacobi(double[][] A, double[] b, double[] xInit, double epsilon = 1e-10, int maxIterations = 500)
        {
            var D = Helper.Diag(Helper.Diag(A));
            var LU = Helper.Sub(A, D);
            var x = xInit;
            var DInv = Helper.Diag(Helper.Inv(Helper.Diag(D)));
            for (var i = 0; i < maxIterations; i++)
            {
                var xNew = Helper.Dot(DInv, Helper.Sub(b, Helper.Dot(LU, x)));
                if(Helper.Norm(Helper.Sub(xNew, x)) < epsilon)
                    return xNew;
                x = xNew;
            }
            return x;
        }
    }
}
