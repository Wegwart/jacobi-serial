﻿using System;

namespace JacobiSerial
{
    public class JacobiTest
    {
        public static void Run()
        {
            Console.WriteLine("JacobiTest.Run...");
            Jacobi_Sample_ErrorCheckOk();
            Console.WriteLine("Passed");
        }

        public static void Jacobi_Sample_ErrorCheckOk()
        {
            // problem data
            var A = new double[][] {
                new double[] { 5, 2, 1, 1},
                new double[] { 2, 6, 2, 1},
                new double[] { 1, 2, 7, 1},
                new double[] { 1, 1, 2, 8},
            };
            var b = new double[] { 29, 31, 26, 19 };
            Console.WriteLine("Let Ax = b");
            Console.WriteLine("A: {0}", Helper.ToString(A));
            Console.WriteLine("b: {0}", Helper.ToString(b));

            Console.WriteLine("Run jacobi...");
            var epsilon = 1e-10;
            var maxIterations = 500;

            // you can choose any starting vector
            var xInit = new double[4] { 0, 0, 0, 0 };
            Console.WriteLine("epsilon: {0}", epsilon);
            Console.WriteLine("maxIterations: {0}", maxIterations);
            Console.WriteLine("xInit: {0}", Helper.ToString(xInit));
            var start_time = DateTime.Now;
            var xResult = JacobiSerial.Jacobi(A, b, xInit, epsilon, maxIterations);
            Console.WriteLine("--- {0} seconds ---", (DateTime.Now - start_time).TotalSeconds);

            var bComputed = Helper.Dot(A, xResult);
            Console.WriteLine("xResult: {0}", Helper.ToString(xResult));
            Console.WriteLine("bComputed: {0}", Helper.ToString(bComputed));
            var bError = Helper.Norm(Helper.Sub(bComputed, b));
            var maxError = 1e-7;
            Console.WriteLine("assert: bError {0} < MaxError {1}", bError, maxError);

            Helper.Assert(bError < maxError);
        }
    }
}
