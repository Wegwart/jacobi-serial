﻿namespace JacobiSerial
{
    class Program
    {
        static void Main(string[] args)
        {
            HelperTest.Run();
            JacobiTest.Run();
        }
    }
}
