# Jacobi Serial

Test Jacobi Method (i5-6200U CPU @ 2.30GHz)


HelperTest.Run...
Passed
JacobiTest.Run...
Let Ax = b
A: [[5 2 1 1]
[2 6 2 1]
[1 2 7 1]
[1 1 2 8]]
b: [29 31 26 19]
Run jacobi...
epsilon: 1E-10
maxIterations: 500
xInit: [0 0 0 0]
--- 0.0069873 seconds ---
xResult: [3.99275362320451 2.95410628020954 2.16183574880456 0.966183574889603]
bComputed: [29.0000000001358 31.000000000165 26.0000000001451 19.00000000014]
assert: bError 2.93812087671639E-10 < MaxError 1E-07
Passed
Press any key to continue . . .