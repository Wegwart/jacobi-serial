from jacobi import *
import numpy as np
import time

def test_jacobi():

    # problem data
    A = np.array([
        [5, 2, 1, 1],
        [2, 6, 2, 1],
        [1, 2, 7, 1],
        [1, 1, 2, 8]
    ])
    b = np.array([29, 31, 26, 19])
    print("Let Ax = b")
    print("A:", A)
    print("b:", b)

    print("Run jacobi...")
    epsilon = 1e-10
    max_iterations = 500

    # you can choose any starting vector
    x_init = np.zeros(len(b))
    print("epsilon:", epsilon)
    print("max_iterations:", max_iterations)
    print("x_init:", x_init)
    start_time = time.time()
    x_result = jacobi(A, b, x_init, epsilon, max_iterations)
    print("--- %s seconds ---" % (time.time() - start_time))

    b_computed = np.dot(A, x_result)
    print("x_result:", x_result)
    print("b_computed:", b_computed)
    b_error = np.linalg.norm(b_computed - b)  # np.sqrt(sum(np.square(b_computed - b)))
    max_error = 1e-8
    print("assert: b_error {0} < max_error {1}".format(b_error, max_error))

    assert np.linalg.norm(b_error) < max_error

if __name__ == '__main__':
    test_jacobi()
    print("Passed")